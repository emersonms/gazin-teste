import { useEffect, useState } from 'react';
import { Box, FormControl, FormHelperText, Grid, InputLabel, LinearProgress, MenuItem, Paper, Select, SelectChangeEvent, Typography } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { DesenvolvedorService } from '../../shared/services/api/desenvolvedores/DesenvolvedorService';
import { VTextField, VForm, useVForm, IVFormErrors } from '../../shared/forms';
import { FerramentasDeDetalhe } from '../../shared/components';
import { LayoutBaseDePagina } from '../../shared/layouts';
import { Label } from '@mui/icons-material';
import { IListarNiveis, NiveisService } from '../../shared/services/api/niveis/NiveisService';
import moment from 'moment';

interface IFormData {
  nome: string;
  nivel: number;
  sexo: string;
  datanascimento: string;
  idade: number;
  hobby: string;
}

const formValidationSchema: yup.SchemaOf<IFormData> = yup.object().shape({
  nome: yup.string().required(),
  nivel: yup.number().required(),
  sexo: yup.string().required(),
  datanascimento: yup.string().required(),
  idade: yup.number().required(),
  hobby: yup.string().required()
});

export const Desenvolvedores: React.FC = () => {
  const { formRef, save, saveAndClose, isSaveAndClose } = useVForm();
  const { id = 'nova' } = useParams<'id'>();
  const navigate = useNavigate();


  const [isLoading, setIsLoading] = useState(false);
  const [nome, setNome] = useState('');
  const [nivel, setNivel] = useState('');
  const [sexo, setSexo] = useState('');
  const [datanascimento, setDatanascimento] = useState('');
  const [idade, setIdade] = useState('');
  const [hobby, setHobby] = useState('');
  const [rows, setRows] = useState<IListarNiveis[]>([]);
  const showButtons = true;

  useEffect(() => {
    if (id !== 'nova') {
      setIsLoading(true);

      DesenvolvedorService.getById(Number(id))
        .then((result) => {
          setIsLoading(false);

          if (result instanceof Error) {
            alert(result.message);
            navigate('/pessoas');
          } else {
            setNome(result.nome);
            result.datanascimento = moment(result.datanascimento).format('DD/MM/Y');
            formRef.current?.setData(result);
          }
        });
    } else {
      formRef.current?.setData({
        email: '',
        nomeCompleto: '',
        cidadeId: undefined,
      });
    }

    NiveisService.getAllWithOutPagination(0, '')
        .then((result) => {
          setIsLoading(false);
          if (result instanceof Error) {
            return false;
          }
          
          setRows(result.data);
        });


  }, [id]);


  const handleSave = (dados: IFormData) => {
    formValidationSchema.
      validate(dados, { abortEarly: false })
      .then((dadosValidados) => {
        setIsLoading(true);

        if (id === 'nova') {
          DesenvolvedorService
            .create(dadosValidados)
            .then((result) => {
              setIsLoading(false);

              if (result instanceof Error) {
                alert(result.message);
              } else {
                if (isSaveAndClose()) {
                  navigate('/desenvolvedores');
                } else {
                  navigate(`/desenvolvedores/detalhe/${result}`);
                }
              }
            });
        } else {
          DesenvolvedorService
            .updateById(Number(id), { id: Number(id), ...dadosValidados })
            .then((result) => {
              setIsLoading(false);

              if (result instanceof Error) {
                alert(result.message);
              } else {
                if (isSaveAndClose()) {
                  navigate('/pessoas');
                }
              }
            });
        }
      })
      .catch((errors: yup.ValidationError) => {
        const validationErrors: IVFormErrors = {};

        errors.inner.forEach(error => {
          if (!error.path) return;

          validationErrors[error.path] = error.message;
        });

        formRef.current?.setErrors(validationErrors);
      });
  };

  return (
    <LayoutBaseDePagina
      titulo={id === 'nova' ? 'Criar Desenvolvedor' : 'Editar Desenvolvedor'}
      barraDeFerramentas={
        <FerramentasDeDetalhe
          mostrarBotaoVoltar={showButtons}
          aoClicarEmVoltar={() => navigate('/desenvolvedores')}
        />
        
      }
    >

      <VForm ref={formRef} onSubmit={handleSave}>
        <Box margin={1} display="flex" flexDirection="column" component={Paper} variant="outlined">

          <Grid container direction="column" padding={2} spacing={2}>

            {isLoading && (
              <Grid item>
                <LinearProgress variant='indeterminate' />
              </Grid>
            )}

            <Grid item>
              <Typography variant='h6'>Geral</Typography>
            </Grid>

            <Grid container item direction="row" spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={4} xl={2}>
                <VTextField
                  fullWidth
                  name='nome'
                  disabled={isLoading}
                  label='Nome'
                  onChange={e => setNome(e.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container item direction="row" spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={4} xl={2}>
                <VTextField
                  select
                  fullWidth
                  name='nivel'
                  disabled={isLoading}
                  label='Nível'
                >
                  {rows.map(Option => (
                    <MenuItem key={Option.id} value={Option.id}>
                      {Option.nivel} ({Option.nivel})
                    </MenuItem>
                  ))}
                </VTextField>
              </Grid>
            </Grid>

            <Grid container item direction="row" spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={4} xl={2}>
                <VTextField
                  fullWidth
                  name='sexo'
                  disabled={isLoading}
                  label='Sexo'
                  onChange={e => setSexo(e.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container item direction="row" spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={4} xl={2}>
                <VTextField
                  fullWidth
                  name='datanascimento'
                  disabled={isLoading}
                  label='Data de Nascimento'
                  onChange={e => setDatanascimento(e.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container item direction="row" spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={4} xl={2}>
                <VTextField
                  fullWidth
                  name='idade'
                  disabled={isLoading}
                  label='Idade'
                  onChange={e => setIdade(e.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container item direction="row" spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={4} xl={2}>
                <VTextField
                  fullWidth
                  name='hobby'
                  disabled={isLoading}
                  label='Hobby'
                  onChange={e => setHobby(e.target.value)}
                />
              </Grid>
            </Grid>

          </Grid>
        </Box>
        <FerramentasDeDetalhe
          mostrarBotaoSalvar={showButtons}
          aoClicarEmSalvar={saveAndClose}
        />
      </VForm>
    </LayoutBaseDePagina>
  );
};
