<?php

namespace App\Interfaces;

interface RepositoryInterface
{
    public function getAll(array $data);
    public function getById($id);
    public function delete($Id);
    public function create(array $data);
    public function update($id, array $data);
}
