<?php

namespace Tests\Feature;

use App\Models\Niveis;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NiveisTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_pegar_todos_niveis()
    {
        Niveis::factory()->create();
        $response = $this->get('api/niveis');
        $this->assertEquals(1, json_decode($response->content())->totalCount);
    }

    public function test_pegar_nivel()
    {
        $nivel = Niveis::factory()->create();
        $response = $this->get('api/niveis/'. $nivel->id);
        $response->assertStatus(200);
    }

    public function test_criar_novo_nivel()
    {
        $response = $this->postJson('api/niveis', ['nivel' => 'CEO']);
        $response->assertStatus(201);
    }

    public function test_editar_nivel()
    {
        $nivel = Niveis::factory()->create();
        $this->putJson('api/niveis/' . $nivel->id, ['nivel' => 'CEO']);
        $response = $this->get('api/niveis/'. $nivel->id);
        $this->assertNotEquals($nivel->nivel, json_decode($response->content())->data->nivel);
    }

    public function test_deletar_nivel()
    {
        $nivelUm = Niveis::factory()->create();
        $nivelDois = Niveis::factory()->create();
        $this->delete('api/niveis/'. $nivelDois->id);
        $countNiveis = count(Niveis::all());
        $this->assertEquals(1, $countNiveis);
    }
}
