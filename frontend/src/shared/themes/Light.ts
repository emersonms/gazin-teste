import { createTheme } from '@mui/material';
import { cyan, yellow } from '@mui/material/colors';

export const LightTheme = createTheme({
  palette: {
    background: {
      paper: '#ffffff',
      default: '#f7f6f3',
    }
  }
});
