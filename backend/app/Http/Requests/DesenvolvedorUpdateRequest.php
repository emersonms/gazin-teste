<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class DesenvolvedorUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nivel' => 'nullable|integer',
            'nome' => 'nullable|string|max:255',
            'sexo' => 'nullable|string',
            'datanascimento' => 'nullable',
            'idade' => 'nullable|integer',
            'hobby' => 'nullable|string|max:255'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json([
            'error' => $validator->errors(),
            'success' => false
        ], Response::HTTP_BAD_REQUEST));
    }
}
