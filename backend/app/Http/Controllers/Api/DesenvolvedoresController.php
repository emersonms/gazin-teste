<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DesenvolvedorRequest;
use App\Http\Requests\DesenvolvedorUpdateRequest;
use App\Http\Requests\NivelRequest;
use App\Repositories\DesenvolvedorRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DesenvolvedoresController extends Controller
{
    private DesenvolvedorRepository $desenvolvedorRepository;

    public function __construct(DesenvolvedorRepository $desenvolvedorRepository)
    {
        $this->desenvolvedorRepository = $desenvolvedorRepository;
    }

    public function index(Request $request)
    {
        try {
            $result = $this->desenvolvedorRepository->getAll($request->all());
            return response()->json([
                'success' => true,
                'data' => $result['desenvolvedores'],
                'totalCount' => $result['totalCount']
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function show($id)
    {
        try {
            $nivel = $this->desenvolvedorRepository->getById($id);
            if (! $nivel) {
                return response()->json(['success' => false, 'error' => 'Nivel not found'], Response::HTTP_NOT_FOUND);
            }
            return response()->json(['success' => true, 'data' => $nivel]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => 'Bad Request'
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function store(DesenvolvedorRequest $request)
    {
        try {
            $createNivel = $this->desenvolvedorRepository->create($request->all());
            if (! $createNivel) {
                return response()->json([
                    'success' => false,
                    'error' => 'Bad Request'
                ], Response::HTTP_BAD_REQUEST);
            }
            return response()->json(['success' => true, 'data' => $createNivel], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(DesenvolvedorUpdateRequest $request, $id)
    {
        try {
            $createNivel = $this->desenvolvedorRepository->update($id, $request->all());
            if (! $createNivel) {
                return response()->json([
                    'success' => false,
                    'error' => 'Bad Request'
                ], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['success' => true, 'data' => $createNivel]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy($id)
    {
        try {
            $nivel = $this->desenvolvedorRepository->getById($id);
            if (! $nivel) {
                return response()->json([
                    'success' => false,
                    'error' => 'Bad Request'
                ], Response::HTTP_BAD_REQUEST);
            }
            $this->desenvolvedorRepository->delete($id);
            return response()->json(['success' => true, 'data' => 'successfully deleted']);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => 'Bad Request'
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
