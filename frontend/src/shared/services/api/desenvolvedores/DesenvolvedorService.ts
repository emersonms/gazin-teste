import { Environment } from '../../../environment';
import { Api } from '../axios-config';


export interface IListarDesenvolvedor {
  id: number;
  nivel: number;
  nome: string;
  sexo: string;
  datanascimento: string;
  idade: number;
  hobby: string;
}

export interface IDesenvolvedor {
  id: number;
  nome: string;
  nivel: number;
  sexo: string;
  datanascimento: string;
  idade: number;
  hobby: string;
}

type TDesenvolvedorComTotalCount = {
  data: IListarDesenvolvedor[];
  totalCount: number;
}

const getAll = async (page = 1, filter = ''): Promise<TDesenvolvedorComTotalCount | Error> => {
  try {
    const urlRelativa = `/desenvolvedores?name=${filter}&page=${page}`;
    console.log(urlRelativa)
    const { data, headers } = await Api.get(urlRelativa);
    if (data) {
      return {
        data: data.data,
        totalCount: Number(data.totalCount),
      };
    }
    return new Error('Erro ao listar os registros.');
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao listar os registros.');
  }
};

const getById = async (id: number): Promise<IDesenvolvedor | Error> => {
  try {
    const { data } = await Api.get(`/desenvolvedores/${id}`);
    console.log(data);
    
    if (data) {
      return data.data;
    }

    return new Error('Erro ao consultar o registro.');
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao consultar o registro.');
  }
};

const create = async (dados: Omit<IDesenvolvedor, 'id'>): Promise<number | Error> => {
  try {
    const { data } = await Api.post<IDesenvolvedor>('/desenvolvedores', dados);

    if (data) {
      return data.id;
    }

    return new Error('Erro ao criar o registro.');
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao criar o registro.');
  }
};

const updateById = async (id: number, dados: IDesenvolvedor): Promise<void | Error> => {
  try {
    await Api.put(`/desenvolvedores/${id}`, dados);
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao atualizar o registro.');
  }
};

const deleteById = async (id: number): Promise<void | Error> => {
  try {
    await Api.delete(`/desenvolvedores/${id}`);
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao apagar o registro.');
  }
};

export const DesenvolvedorService = {
  getAll,
  create,
  getById,
  updateById,
  deleteById,
};
