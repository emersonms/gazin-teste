# Teste Gazin

# Subindo os containers

docker-compose up -d --build

# instalando dependências do backend

docker exec -it gazin-backend bash <br>
composer install <br>
php artisan migrate <br>

# instalando as dependências do frontend

docker exec -it gazin-frontend bash <br>
npm install <br>

# executando a aplicação frontend

docker exec -it gazin-frontend bash <br>
npm start <br>

# executando a tests unitários

docker exec -it gazin-backend bash <br>
./vendor/bin/pest <br>


# url da aplicação

http://localhost:3000/ 

# url da API

http://localhost:9002/ 

# Dados do Banco

0.0.0.0 porta 3008 <br>
user: gazin <br>
password: gazin <br>
database: gazin <br>


