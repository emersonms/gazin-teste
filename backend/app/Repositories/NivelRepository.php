<?php

namespace App\Repositories;

use App\Interfaces\RepositoryInterface;
use App\Models\Desenvolvedores;
use App\Models\Niveis;
use Illuminate\Http\Response;

class NivelRepository implements RepositoryInterface
{
    public function getAll(array $data)
    {
        if (isset($data['page']) && $data['page'] == 0) {
            return $this->getAllWithOutPagination();
        }

        $page = $data['page'] ?? 0;
        $page = $page > 0 ? $page-1 : 0;
        $limit = $data['limit'] ?? 10;
        $skip = $limit * $page;
        if (isset($data['nivel'])) {
            $niveis = Niveis::where('nivel', 'like', '%' . $data['nivel'] . '%')
                ->skip($skip)
                ->take($limit)
                ->get();
            return [ 'niveis' => $niveis, 'totalCount' => count(Niveis::where('nivel', 'like', '%' . $data['nivel'] . '%')->get())];
        }

        $niveis = Niveis::skip($skip)
            ->take($limit)
            ->get();
        return [ 'niveis' => $niveis, 'totalCount' => count(Niveis::all())];
    }

    private function getAllWithOutPagination()
    {
        return [ 'niveis' => Niveis::all(), 'totalCount' => count(Niveis::all())];
    }

    public function getById($id)
    {
        return Niveis::find($id);
    }

    public function delete($id)
    {
        $desenvolvedores = Desenvolvedores::where(['nivel' => $id])->first();
        if ($desenvolvedores) {
            throw new \Exception('there are developers associated with this level', Response::HTTP_NOT_IMPLEMENTED);
        }
        Niveis::destroy($id);
    }

    public function create(array $data)
    {
        return Niveis::create($data);
    }

    public function update($id, array $data)
    {
        try {
            Niveis::whereId($id)->update($data);
            return Niveis::findOrFail($id);
        } catch (\Exception $e) {
            return null;
        }
    }
}
