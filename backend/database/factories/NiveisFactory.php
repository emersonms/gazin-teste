<?php

namespace Database\Factories;

use App\Models\Niveis;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Niveis>
 */
class NiveisFactory extends Factory
{
    protected $model = Niveis::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $niveis = ['Junior', 'Pleno', 'Senior', 'Arquiteto', 'Gerente'];
        $nivel = $niveis[array_rand($niveis)];
        return [
            'nivel' => $nivel
        ];
    }
}
