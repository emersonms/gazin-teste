import { useEffect } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import { useDrawerContext } from '../shared/contexts';
import {
  ListarDesenvolvedores,
  Desenvolvedores,
  ListarNiveis,
  Niveis
} from '../pages';

export const AppRoutes = () => {
  const { setDrawerOptions } = useDrawerContext();

  useEffect(() => {
    setDrawerOptions([
      {
        icon: 'people',
        path: '/desenvolvedores',
        label: 'Desenvolvedores',
      },
      {
        icon: 'pages',
        path: '/niveis',
        label: 'Níveis',
      }
      
    ]);
  }, []);

  return (
    <Routes>
      <Route path="/desenvolvedores" element={<ListarDesenvolvedores />} />
      <Route path="/desenvolvedores/detalhe/:id" element={<Desenvolvedores />} />

      <Route path="/niveis" element={<ListarNiveis />} />
      <Route path="/niveis/detalhe/:id" element={<Niveis />} />

      <Route path="*" element={<Navigate to="/desenvolvedores" />} />
    </Routes>
  );
};
