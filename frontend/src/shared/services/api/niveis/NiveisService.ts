import { Environment } from '../../../environment';
import { Api } from '../axios-config';


export interface IListarNiveis {
  id: number;
  nivel: string;
}

export interface INivel {
  id: number;
  nivel: string;
}

type TNiveisComTotalCount = {
  data: IListarNiveis[];
  totalCount: number;
}

const getAll = async (page = 1, filter = ''): Promise<TNiveisComTotalCount | Error> => {
  try {
    const urlRelativa = `/niveis?nivel=${filter}&page=${page}`;
    const { data, headers } = await Api.get(urlRelativa);
    if (data) {
      return {
        data: data.data,
        totalCount: Number(data.totalCount),
      };
    }

    return new Error('Erro ao listar os nível.');
  } catch (error) {
    return new Error((error as { message: string }).message || 'Erro ao listar os nível.');
  }
};

const getAllWithOutPagination = async (page = 0, filter = ''): Promise<TNiveisComTotalCount | Error> => {
  try {
    const urlRelativa = `/niveis?page=${page}`;
    const { data, headers } = await Api.get(urlRelativa);
    if (data) {
      return {
        data: data.data,
        totalCount: Number(data.totalCount),
      };
    }

    return new Error('Erro ao listar os nível.');
  } catch (error) {
    return new Error((error as { message: string }).message || 'Erro ao listar os nível.');
  }
};

const getById = async (id: number): Promise<INivel | Error> => {
  try {
    const { data } = await Api.get(`/niveis/${id}`);

    if (data) {
      return data.data;
    }

    return new Error('Erro ao consultar o nível.');
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao consultar o nível.');
  }
};

const create = async (dados: Omit<INivel, 'id'>): Promise<number | Error> => {
  try {
    const { data } = await Api.post('/niveis', dados);
    if (data) {
      return data.data.id;
    }

    return new Error('Erro ao criar o nível.');
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao criar o nível.');
  }
};

const updateById = async (id: number, dados: INivel): Promise<void | Error> => {
  try {
    await Api.put(`/niveis/${id}`, dados);
  } catch (error) {
    console.error(error);
    return new Error((error as { message: string }).message || 'Erro ao atualizar o nível.');
  }
};

const deleteById = async (id: number): Promise<void | Error> => {
  try {
    await Api.delete(`/niveis/${id}`);
  } catch (error) {
    console.error(error);
    return new Error('Nível não pode ser deletado.');
  }
};


export const NiveisService = {
  getAll,
  getAllWithOutPagination,
  create,
  getById,
  updateById,
  deleteById,
};
