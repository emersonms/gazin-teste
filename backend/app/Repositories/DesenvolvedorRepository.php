<?php

namespace App\Repositories;

use App\Interfaces\RepositoryInterface;
use App\Models\Desenvolvedores;
use Carbon\Carbon;

class DesenvolvedorRepository implements RepositoryInterface
{
    public function getAll(array $data)
    {
        $page = $data['page'] ?? 0;
        $page = $page > 0 ? $page-1 : 0;
        $limit = $data['limit'] ?? 10;
        $skip = $limit * $page;
        if (isset($data['name'])) {
            $desenvolvedores = Desenvolvedores::with('niveis')
                ->where('nome', 'like', '%' . $data['name'] . '%')
                ->skip($skip)
                ->take($limit)
                ->get();
            return [
                'desenvolvedores' => $desenvolvedores,
                'totalCount' => count(Desenvolvedores::where('nome', 'like', '%' . $data['name'] . '%')
                    ->get())
            ];
        }

        $desenvolvedores = Desenvolvedores::with('niveis')
            ->skip($skip)
            ->take($limit)
            ->get();
        return [ 'desenvolvedores' => $desenvolvedores, 'totalCount' => count(Desenvolvedores::all())];
    }

    public function getById($id)
    {
        return Desenvolvedores::find($id);
    }

    public function delete($id)
    {
        Desenvolvedores::destroy($id);
    }

    public function create(array $data)
    {
        if (isset($data['datanascimento'])) {
            $data['datanascimento'] = Carbon::createFromFormat('d/m/Y', $data['datanascimento'])->format('Y-m-d');
        }
        return Desenvolvedores::create($data);
    }

    public function update($id, array $data)
    {
        try {
            if (isset($data['datanascimento'])) {
                $data['datanascimento'] = Carbon::createFromFormat('d/m/Y', $data['datanascimento'])->format('Y-m-d');
            }
            Desenvolvedores::whereId($id)->update($data);
            return Desenvolvedores::findOrFail($id);
        } catch (\Exception $e) {
            return null;
        }
    }
}
