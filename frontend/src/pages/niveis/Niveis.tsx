import { useEffect, useState } from 'react';
import { Box, Grid, LinearProgress, Paper, Typography } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import * as yup from 'yup';
import { NiveisService } from '../../shared/services/api/niveis/NiveisService';
import { VTextField, VForm, useVForm, IVFormErrors } from '../../shared/forms';
import { FerramentasDeDetalhe } from '../../shared/components';
import { LayoutBaseDePagina } from '../../shared/layouts';

interface IFormData {
  nivel: string;
}

const formValidationSchema: yup.SchemaOf<IFormData> = yup.object().shape({
  nivel: yup.string().required().min(3),
});

export const Niveis: React.FC = () => {
  const { formRef, save, saveAndClose, isSaveAndClose } = useVForm();
  const { id = 'nova' } = useParams<'id'>();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [nivel, setNivel] = useState('');
  const showButtons = true;

  useEffect(() => {
    if (id !== 'nova') {
      setIsLoading(true);
      NiveisService.getById(Number(id))
        .then((result) => {
          setIsLoading(false);
          if (result instanceof Error) {
            alert(result.message);
            return navigate('/niveis');
          }

          setNivel(result.nivel);
          formRef.current?.setData(result);
        });
        return;
    }

    formRef.current?.setData({
      nome: '',
    });
  }, [id]);


  const handleSave = (dados: IFormData) => {
    formValidationSchema.
      validate(dados, { abortEarly: false })
      .then((dadosValidados) => {
        setIsLoading(true);
        if (id === 'nova') {
          return NiveisService
            .create(dadosValidados)
            .then((result) => {
              setIsLoading(false);
              if (result instanceof Error) {
                return alert(result.message);
              }
              return navigate('/niveis');
            });
        }

        return NiveisService.updateById(Number(id), { id: Number(id), ...dadosValidados })
          .then((result) => {
            setIsLoading(false);
          if (result instanceof Error) {
            alert(result.message);
          }
          navigate('/niveis');
        });
      })
      .catch((errors: yup.ValidationError) => {
        const validationErrors: IVFormErrors = {};
        errors.inner.forEach(error => {
          if (!error.path) return;
          validationErrors[error.path] = error.message;
        });
        formRef.current?.setErrors(validationErrors);
      });
  };

  return (
    <LayoutBaseDePagina
      titulo={id === 'nova' ? 'Criar Nível' : 'Editar Nível'}
      barraDeFerramentas={
        <FerramentasDeDetalhe
          mostrarBotaoVoltar={showButtons}
          aoClicarEmVoltar={() => navigate('/niveis')}
        />
        
      }
    >
      <VForm ref={formRef} onSubmit={handleSave}>
        <Box margin={1} display="flex" flexDirection="column" component={Paper} variant="outlined">
          <Grid container direction="column" padding={2} spacing={2}>
            {isLoading && (
              <Grid item>
                <LinearProgress variant='indeterminate' />
              </Grid>
            )}
            <Grid item>
              <Typography variant='h6'>Geral</Typography>
            </Grid>
            <Grid container item direction="row" spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={4} xl={2}>
                <VTextField
                  fullWidth
                  name='nivel'
                  disabled={isLoading}
                  onChange={e => setNivel(e.target.value)}
                />
              </Grid>
            </Grid>
          </Grid>
        </Box>
        <FerramentasDeDetalhe
          mostrarBotaoSalvar={showButtons}
          aoClicarEmSalvar={save}
        />
      </VForm>
    </LayoutBaseDePagina>
  );
};
