<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desenvolvedores extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'nivel',
        'nome',
        'sexo',
        'datanascimento',
        'idade',
        'hobby'
    ];

    public function niveis()
    {
        return $this->hasOne(Niveis::class, 'id', 'nivel');
    }
}
