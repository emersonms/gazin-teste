<?php

namespace Tests\Feature;

use App\Models\Desenvolvedores;
use App\Models\Niveis;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DesenvolvedoresTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_pegar_todos_desenvolvedores()
    {
        Desenvolvedores::factory()->create();
        $response = $this->get('api/desenvolvedores');
        $this->assertEquals(1, json_decode($response->content())->totalCount);
    }

    public function test_pegar_desenvolvedor()
    {
        $desenvolvedor = Desenvolvedores::factory()->create();
        $response = $this->get('api/desenvolvedores/'. $desenvolvedor->id);
        $response->assertStatus(200);
    }

    public function test_criar_novo_desenvolvedor()
    {
        $response = $this->postJson('api/desenvolvedores', [
            'nivel' => Niveis::factory()->create()->id,
            'nome' => 'Desenvolvedor Teste',
            'sexo' => 'M',
            'datanascimento' => '10/07/1966',
            'idade' => 30,
            'hobby' => 'Cinema'
        ]);

        $response->assertStatus(201);
    }

    public function test_editar_desenvolvedor()
    {
        $desenvolvedor = Desenvolvedores::factory()->create();
        $this->putJson('api/desenvolvedores/' . $desenvolvedor->id, ['nome' => 'Carlos']);
        $response = $this->get('api/desenvolvedores/'. $desenvolvedor->id);
        $this->assertNotEquals($desenvolvedor->nome, json_decode($response->content())->data->nome);
    }

    public function test_deletar_nivel()
    {
        $desenvolvedorUm = Desenvolvedores::factory()->create();
        $desenvolvedorDois = Desenvolvedores::factory()->create();
        $this->delete('api/desenvolvedores/'. $desenvolvedorDois->id);
        $countNiveis = count(Desenvolvedores::all());
        $this->assertEquals(1, $countNiveis);
    }
}
