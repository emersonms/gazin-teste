<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NivelRequest;
use App\Repositories\NivelRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NiveisController extends Controller
{
    private NivelRepository $nivelRepository;

    public function __construct(NivelRepository $nivelRepository)
    {
        $this->nivelRepository = $nivelRepository;
    }

    public function index(Request $request)
    {
        try {
            $result = $this->nivelRepository->getAll($request->all());
            return response()->json([
                'success' => true,
                'data' => $result['niveis'],
                'totalCount' => $result['totalCount']
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function show($id)
    {
        try {
            $nivel = $this->nivelRepository->getById($id);
            if (! $nivel) {
                return response()->json(['success' => false, 'error' => 'Nivel not found'], Response::HTTP_NOT_FOUND);
            }
            return response()->json(['success' => true, 'data' => $nivel]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => 'Bad Request'
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function store(NivelRequest $request)
    {
        try {
            $createNivel = $this->nivelRepository->create($request->all());
            if (! $createNivel) {
                return response()->json([
                    'success' => false,
                    'error' => 'Bad Request'
                ], Response::HTTP_BAD_REQUEST);
            }
            return response()->json(['success' => true, 'data' => $createNivel], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(NivelRequest $request, $id)
    {
        try {
            $createNivel = $this->nivelRepository->update($id, $request->all());
            if (! $createNivel) {
                return response()->json([
                    'success' => false,
                    'error' => 'Bad Request'
                ], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['success' => true, 'data' => $createNivel]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy($id)
    {
        try {
            $nivel = $this->nivelRepository->getById($id);
            if (! $nivel) {
                return response()->json(['success' => false, 'error' => 'Bad Request'], Response::HTTP_BAD_REQUEST);
            }
            $this->nivelRepository->delete($id);
            return response()->json(['success' => true, 'data' => 'successfully deleted']);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], $e->getCode());
        }
    }
}
