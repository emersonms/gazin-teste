FROM wyveo/nginx-php-fpm:latest

COPY ./backend /usr/share/nginx/html
COPY ./backend/nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html

RUN apt update; \
    apt install vim -y

EXPOSE 80