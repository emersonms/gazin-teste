<?php

namespace Database\Factories;

use App\Models\Desenvolvedores;
use App\Models\Niveis;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Desenvolvedores>
 */
class DesenvolvedoresFactory extends Factory
{
    protected $model = Desenvolvedores::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nivel' => Niveis::factory()->create()->id,
            'nome' => 'Desenvolvedor Teste',
            'sexo' => 'M',
            'datanascimento' => '1996-07-10',
            'idade' => 30,
            'hobby' => 'Cinema'
        ];
    }
}
